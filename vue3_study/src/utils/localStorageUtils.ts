import {Todo} from "@/types/Todo";

// 保存数据到浏览器的缓存
export function saveTodos(todos: Todo []) {
    localStorage.setItem('todos', JSON.stringify(todos))
}

export function getTodos(): Todo [] {
    return JSON.parse(localStorage.getItem('todos') || '[]')
}